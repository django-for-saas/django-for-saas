# django-for-saas

Course about building multitenant web application with paid features.
 
## For whom is this course:
- Those who are relatively new to software development.
- Those who are experienced developers, but want to improve their skills with a guided course.
 
In either case knowledge at least basics of Python and HTML, CSS and understanding browser/server developement is required.


## What we'll build in this course:
We will build a very simple landing page building service.

Why are we building another landing page service?

It is still very popular type of SaaS that appears every day with their own set of features and approach that makes it appealling for a certain niche.

It is complex enough for this course.

It is understandable type of project for wide enough audience. I didn't want to choose a very specific type of project that is used only in very rare industry/niche.

## What you will learn:

### Part 1
You will learn how to setup the backend part of the application.

You will learn about models, authentication, multi-tenancy.

By the end of this part you will have basic app where users can register, login, create projects(sites) and pages in it with basic structure.

### Part 2
In this part we are making a simple builder interface for landing page.

### Part 3
In this part we are making our public side of the app


### Part 4 (Premium)
We work on paid accounts, plans, paid features

### Part 5(Premium)
Custom domains:

- adding domain to the project
- checking the domain setup correctly
- CNAME, A records, DNS checking

### Part 6 (Premium)
Improving performance and scaling of our app:
CDN, S3 for file storage

### Part 7 (Premium)
We build angular interface for page builder for better user experience

### Part 8 (Premium)
TBD. Possibly:
- making automatic OG images
- building public API

 
## Course will consist of two parts
- Free part where you will learn how to build basic working version of a landing page service
- Paid part where I will guide you through building advanced features (TBD) Custom domains, paid accounts with Paddle, limiting features according to plan.
 
## Format
Course will be both in text and video formats.

## Requirements:
- Some python knowledge.
- Basics of how web apps work: browser, server, HTTP and HTTPS, headers.
- Basics of HTML, CSS and JavaScript.
- Pycharm(I rely a lot on this IDE, but you can work with any other editor)
- GitHub account
- Digital ocean or AWS account
- Docker installed on your computer.



## Table of Contents:

### Part 1:
- [Introduction](part-1/01-introduction.md)
- [Changelog](part-1/02-changelog.md)
- [Getting Started](part-1/03-getting-started.md)



