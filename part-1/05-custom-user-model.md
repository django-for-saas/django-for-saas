# Users and Authentication

Django has a lot of code for user management and authentication out of the box.

This allows for a quick start, but we want to make certain changes to user model to better fit our use case.


## Custom User Model
We need to make certain changes to the user model.

We can't change anything in built-in user model, so we have to create our own and make our project use it.

It is important to change user model to a custom one in the beginning of the project.

Changing it for ongoing project with data is a huge pain.

Standard user model has a username field that is used for login.

It is common to have an email field as a login and that's what we need to do.

The EmailField in Django is case-insensitive. We need to make email field case-insensitive so that `EmAil@example.com` and `email@example.com` will be effectively the same emails.

In order to create a custom user model we need an app to put it to.

Let's create our app.

Please note that most of our commands we are running through docker.

In terminal, in the root of our django project run this command to create an app:

```bash
docker-compose run web python manage.py startapp landing_pages
```

Now we need to edit `django_for_saas_app/settings.py` and add out application.

Add `landing_apges` to the `INSTALLED_APPS` list:
```

INSTALLED_APPS = [
    # ....
    'landing_pages',
]
```

In `landing_pages/models.py` add User class with all required imports:

```python
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.contrib.postgres.fields import CIEmailField
from landing_pages.managers import UserManager
from django.core.mail import send_mail
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils import timezone


class User(AbstractBaseUser, PermissionsMixin):
    """
    User class for the project with email field as login
    """
    email = CIEmailField(
        _('email address'),
        blank=True,
        unique=True,
        error_messages={
            'unique': _("A user with that email already exists."),
        }, )
    name = models.CharField(_('name'), max_length=255, blank=True)
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    is_email_confirmed = models.BooleanField(_('email confirmed'), default=False)
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    objects = UserManager()

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        return self.name.strip()

    def get_short_name(self):
        """Return the short name for the user."""
        return self.name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """Send an email to this user."""
        send_mail(subject, message, from_email, [self.email], **kwargs)

```
Create a file `landing_pages/managers.py`:

```python
from django.contrib.auth.base_user import BaseUserManager


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Create and save a user with the given username, email, and password.
        """
        if not email:
            raise ValueError('The given username must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email=None, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        return self._create_user(email, password, **extra_fields)
```

You can read more about managers here: [Managers in Django Docs](https://docs.djangoproject.com/en/3.1/topics/db/managers/)

Why we need a custom manager? Because we changed the username field, and built-in `UserManager` expects us to have a `username` field.


In `settings.py` specify our user model for the project:

```python
AUTH_USER_MODEL = 'landing_pages.User'
```

Now we need to create migrations for our model. [Read more about migrations in Django docs](https://docs.djangoproject.com/en/3.1/topics/migrations/)

Run this to create migrations:

```bash
docker-compose run web python manage.py makemigrations
```

Now we need to edit the created migration file to add extension `CITextExtension`.

Open file `landing_pages/migrations/0001_initial.py`.

Add the following import after existing imports

```python
from django.contrib.postgres.operations import CITextExtension
```

In the class `Migration` find the `operations` list and as the first element add CITextExtension() element, so the class looks like this

```python
class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0012_alter_user_first_name_max_length'),
    ]

    operations = [
        CITextExtension(),
        migrations.CreateModel(
            name='User',
            fields=[
                .....
                .....
                ....

```

Now we can apply migrations:

```bash
docker-compose run web python manage.py migrate
```


The output should be close to this, if everything went fine:

```bash
✔ ~/src/django_for_saas_app 
19:15 $ docker-compose run web python manage.py migrate
Starting django_for_saas_app_rabbitmq_1 ... done
Starting django_for_saas_app_redis_1    ... done
Starting django_for_saas_app_db_1       ... done
Operations to perform:
  Apply all migrations: admin, auth, contenttypes, landing_pages, sessions
Running migrations:
  Applying landing_pages.0001_initial... OK
  Applying admin.0001_initial... OK
  Applying admin.0002_logentry_remove_auto_add... OK
  Applying admin.0003_logentry_add_action_flag_choices... OK
  Applying sessions.0001_initial... OK
✔ ~/src/django_for_saas_app 
19:25 $ 
```


To be able to manage users from Django admin we need to register our `User` model in `landing_pages/admin.py`.

Paste this code to replace contents of `admin.py`:

```python
from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from landing_pages.models import User
from django.contrib.auth.admin import UserAdmin as DefaultUserAdmin


@admin.register(User)
class UserAdmin(DefaultUserAdmin):
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),
        }),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
        ('User data',
         {'fields': (
             ('is_email_confirmed',),
         )}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )
    list_display = ('email', 'name', 'is_staff')
    search_fields = ('name', 'email')
    ordering = ('email',)

```

Time to create our admin user.

Run this command and answer the questions:

```bash
docker-compose run web python manage.py createsuperuser
```

With output it should look like this:

```bash
19:37 $ docker-compose run web python manage.py createsuperuser
Starting django_for_saas_app_rabbitmq_1 ... done
Starting django_for_saas_app_redis_1    ... done
Starting django_for_saas_app_db_1       ... done
Email address: admin@example.com
Name: Admin
Password: 
Password (again): 
Superuser created successfully.
✔ ~/src/django_for_saas_app 
19:48 $ 

```

## Authentication
