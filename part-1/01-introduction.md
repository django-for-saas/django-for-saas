# Introduction

What is SaaS?

Wikipedia defines SaaS as a software that is hosted by service provider and licensed on a subscription basis.

The app that we are going to build through this course is exactly this:
- An app that makes it possible to build a landing page
- An app that makes resulting landing page accessable to visitors
- The builder will be available for registered user, some of the features available only for paid plans
- The app is hosted and managed by service owner
- Users of the app need to have a browser and subscription to use the service


Objectives:
By the end of Part 1, you will be able to:
1. Register in the app
2. Create a Project
3. Create Pages in the project


Next: [Changelog](./02-changelog.md)