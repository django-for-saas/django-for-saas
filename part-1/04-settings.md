# Settings

Landing pages are supposed to be serving a number of visitors.

There is no use of a landing page (service) that falls because there is outage due to high volume of visitors.

We want our app to be built properly and scalable.

It needs to be ready for the front page of Hacker News and N1 on Product Hunt and survive popularity.

Don't forget that you customers can be end up on those websites too, maybe all at once. Servers and infrastructure is definetely part of another conversation, but architecture of the app must allow scaling.

One of the important steps to do that is to follow rules of The 12-factor app: https://12factor.net/

I recommend to go to that site and read it through before continuing.

Let's list the key points here:

I. Codebase

One codebase tracked in revision control, many deploys

II. Dependencies

Explicitly declare and isolate dependencies

III. Config

Store config in the environment

IV. Backing services

Treat backing services as attached resources

V. Build, release, run

Strictly separate build and run stages

VI. Processes

Execute the app as one or more stateless processes

VII. Port binding

Export services via port binding

VIII. Concurrency

Scale out via the process model

IX. Disposability

Maximize robustness with fast startup and graceful shutdown

X. Dev/prod parity

Keep development, staging, and production as similar as possible

XI. Logs

Treat logs as event streams

XII. Admin processes

Run admin/management tasks as one-off processes

Also there are additional apps we will add in settings that make developing and maintaining the app easier.

With all this in mind let's open our settings file: `django_for_saas_app/settings.py`.

And we remove everything from there and start building our own from scratch.

First we import a couple of libraries, set our root path of the project and create an `env` variable.

```python
from pathlib import Path
import environ

BASE_DIR = Path(__file__).resolve(strict=True).parent.parent
env = environ.Env()

```

`BASE_DIR` we need to set proper paths to places in our project.

`env` will help us properly get configuration from environment variables.


```python
DEBUG = env.bool("DJANGO_DEBUG", False)
```

`DEBUG` should be off at all times except local development environment.


```python
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # 3rd party
    'import_export',
    'django_extensions',
    'rest_framework',
    'corsheaders',
    'djangoql',
]

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'django_for_saas_app.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'django_for_saas_app.wsgi.application'
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

STATICFILES_STORAGE = 'django_for_saas_app.storage.WhiteNoiseStaticFilesStorage'
WHITENOISE_USE_FINDERS = True
if DEBUG:
    WHITENOISE_AUTOREFRESH = True
```


This part is something that we rarely change, so let's put it here first and the rest of the settings will go after.


```python
TIME_ZONE = 'UTC'
LANGUAGE_CODE = 'en-us'
SITE_ID = 1
USE_I18N = True
USE_L10N = True
USE_TZ = True
```

- Choices for timezones: http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
- `LANGUAGE_CODE`: https://docs.djangoproject.com/en/dev/ref/settings/#language-code
- `SITE_ID`: https://docs.djangoproject.com/en/dev/ref/settings/#site-id
- `USE_I18N`: https://docs.djangoproject.com/en/dev/ref/settings/#use-i18n
- `USE_L10N`: https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
- `USE_TZ`: https://docs.djangoproject.com/en/dev/ref/settings/#use-tz

```python
SECRET_KEY = env('DJANGO_SECRET_KEY')
if DEBUG:
    ALLOWED_HOSTS = ['*']
else:
    ALLOWED_HOSTS = env.list('DJANGO_ALLOWED_HOSTS', default=['example.com'])

```

`SECRET_KEY` for non-local development environments, especially in production. Don't reuse them between environments (dev/staging/production).

Read more about `ALLOWED_HOSTS` here: https://docs.djangoproject.com/en/dev/ref/settings/#allowed-hosts

```python
DATABASES = {
    "default": env.db("DATABASE_URL")
}
DATABASES["default"]["ATOMIC_REQUESTS"] = True
DATABASES["default"]["CONN_MAX_AGE"] = env.int("CONN_MAX_AGE", default=60)
```

Database credentials will be passed via URL like `postgresql://username:password@hostname:1234/database`.

```python
ADMIN_URL = env('DJANGO_ADMIN_URL', default='admin/')
```

For a purpose of improving your security a bit it is best to have admin panel on a non-standard URL.

Let's add caching settings:

```python
REDIS_URL = env('REDIS_URL')
CACHES = {
    "default": env.cache('REDIS_URL')
}
```
Final touch is static configuration:

```python
STATIC_ROOT = BASE_DIR / 'staticfiles'
STATIC_HOST = env('DJANGO_STATIC_HOST', default='')
STATIC_URL = STATIC_HOST + '/static/'
```


The resulting `settings.py` should look like this:

```python
from pathlib import Path
import environ

BASE_DIR = Path(__file__).resolve(strict=True).parent.parent
env = environ.Env()

DEBUG = env.bool("DJANGO_DEBUG", False)

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # 3rd party
    'import_export',
    'django_extensions',
    'rest_framework',
    'corsheaders',
    'djangoql',
]

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'django_for_saas_app.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'django_for_saas_app.wsgi.application'
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

STATICFILES_STORAGE = 'django_for_saas_app.storage.WhiteNoiseStaticFilesStorage'
WHITENOISE_USE_FINDERS = True
if DEBUG:
    WHITENOISE_AUTOREFRESH = True

TIME_ZONE = 'UTC'
LANGUAGE_CODE = 'en-us'
SITE_ID = 1
USE_I18N = True
USE_L10N = True
USE_TZ = True

SECRET_KEY = env('DJANGO_SECRET_KEY')
if DEBUG:
    ALLOWED_HOSTS = ['*']
else:
    ALLOWED_HOSTS = env.list('DJANGO_ALLOWED_HOSTS', default=['example.com'])

DATABASES = {
    "default": env.db("DATABASE_URL")  # noqa F405
}
DATABASES["default"]["ATOMIC_REQUESTS"] = True  # noqa F405
DATABASES["default"]["CONN_MAX_AGE"] = env.int("CONN_MAX_AGE", default=60)  # noqa F405

ADMIN_URL = env('DJANGO_ADMIN_URL', default='admin/')

REDIS_URL = env
CACHES = {
    "default": env.cache('REDIS_URL')
}

STATIC_ROOT = BASE_DIR / 'staticfiles'
STATIC_HOST = env('DJANGO_STATIC_HOST', default='')
STATIC_URL = STATIC_HOST + '/static/'

```

Now in the same folder where `settings.py` resides create a file `storage.py`

Put this in this file:

```python
from whitenoise.storage import CompressedManifestStaticFilesStorage


class WhiteNoiseStaticFilesStorage(CompressedManifestStaticFilesStorage):
    manifest_strict = False
```

Learn more about WhiteNoise: http://whitenoise.evans.io/en/stable/



## .env file

In the root of the project create the `.env` file with the following content:

```bash
DJANGO_DEBUG=True
DJANGO_SECRET_KEY=123123123
DATABASE_URL=psql://djangoforsaas:djangoforsaas@db/djangoforsaas
REDIS_URL=redis://redis/0
```



Now you can launch `docker-compose up` and when it started open your browser at https://127.0.0.1:8090/

You should see Django's default screen saying "The install worked successfully! Congratulations!" with a nice rocket taking off!

