# Getting started

Start by creating a project

```bash
python3 -m venv ~/env-django-for-saas-app
pip install \
       Django==3.1.1 \
       gunicorn==20.0.4 \
       Pillow==7.2.0 \
       whitenoise==5.2.0 \
       django-redis==4.12.1 \
       pytz==2020.1 \
       psycopg2-binary==2.8.5 \
       arrow==0.16.0 \
       djangorestframework==3.11.1 \
       django-environ==0.4.5 \
       django-storages==1.10 \
       django-cors-headers==3.5.0 \
       django-braces==1.14.0 \
       django-extensions==3.0.6 \
       celery==4.4.7 \
       boto3==1.14.52 \
       django-ses==1.0.2 \
       djangoql==0.14.0

django-admin startproject django_for_saas_app
cd django_for_saas_app/
```

Here is the file structure of the new project:

```
.
├── django_for_saas_app
│   ├── __init__.py
│   ├── asgi.py
│   ├── settings.py
│   ├── urls.py
│   └── wsgi.py
└── manage.py

```

At this step I open Pycharm. On Mac:

```bash
open -a Pycharm .
```

Let's create our docker-compose and point Pycharm to use Python interpreter from docker-compose.

Create `Dockerfile` in the root of our project:

```Dockerfile
FROM python:3.8
ENV PIP_NO_CACHE_DIR off
ENV PIP_DISABLE_PIP_VERSION_CHECK on
ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 0
RUN apt-get update \
 && apt-get install -y --force-yes \
 nano python-pip gettext chrpath libssl-dev libxft-dev \
 libfreetype6 libfreetype6-dev  libfontconfig1 libfontconfig1-dev\
  && rm -rf /var/lib/apt/lists/*
WORKDIR /code/
COPY requirements.txt /code/
RUN pip install -r requirements.txt
COPY . /code/
```

Create `docker-compose.yml`:

```YML
version: '3.3'
services:
  redis:
    image: redis
    command: redis-server
    ports:
      - "14000:6379"
  rabbitmq:
    image: rabbitmq
    environment:
      - RABBITMQ_DEFAULT_USER=djangoforsaas
      - RABBITMQ_DEFAULT_PASS=djangoforsaas
      - RABBITMQ_DEFAULT_VHOST=djangofarsaas
    volumes:
      - "~/volumes/django_for_saas_app/rabbitmq:/var/lib/rabbitmq"
    ports:
      - "14001:5672"
      - "14002:15672"
  db:
    image: postgres
    environment:
      - POSTGRES_USER=djangofarsaas
      - POSTGRES_PASSWORD=djangofarsaas
      - POSTGRES_DB=djangofarsaas
    volumes:
      - "~/volumes/django_for_saas_app/postgres_data:/var/lib/postgresql/data"
    ports:
      - "14003:5432"

  web:
    build:
      context: .
      dockerfile: Dockerfile
    restart: always
    command: bash -c "sleep 5 && python manage.py runserver 0.0.0.0:8090"
    env_file:
      - .env
    ports:
      - "127.0.0.1:8090:8090"
    volumes:
      - .:/code
    links:
      - db
      - redis
      - rabbitmq
    depends_on:
      - db
      - redis
      - rabbitmq
```

`docker-compose.yml` contains services that are needed to run the project.

`db` is the postgres database service.

`redis` is the redis service, we will be using it for cache purposes, so we don't configure it to persist data, nor add any volume.

`rabbitmq` is the message queue, we'll be using it for celery tasks.

`web` service is the one to run django development server.

One word on the `command` in `web` service. Since our app is starting faster than the postgresql service is ready to accept connections, `runserver` will fail. I have added 5 second delay between start to address this.

This might look like a duct tape. But proper options, that really check for DB to be ready will require additional tools to be installed and/or little more code and I wanted to avoid all that. This is the local development deployment after all and we don't have to make things perfect here.

The better way could be separate docker-compose for databases, but again - it only increases complexity, which I want to avoid here.

Create `requirements.txt`

```
Django==3.1.1
gunicorn==20.0.4
Pillow==7.2.0
whitenoise==5.2.0
django-redis==4.12.1
pytz==2020.1
psycopg2-binary==2.8.5
arrow==0.16.0
djangorestframework==3.11.1
django-environ==0.4.5
django-import-export==2.3.0
django-storages==1.10
django-cors-headers==3.5.0
django-braces==1.14.0
django-extensions==3.0.6
celery==4.4.7
boto3==1.14.52
django-ses==1.0.2
djangoql==0.14.0
```

In order for our docker-compose to run we need to create an empty file: `.env`

It is a text file where we'll put our environment variables, so we don't have to hardcode them into the source code files.

Now we can build our image by running `docker-compose build`.

If everything was built fine, the last two lines will look like this:

```
Successfully built 59ff9daf379f
Successfully tagged django_for_saas_app_web:latest
```

Let's make sure our app is running:

```bash
docker-compose up
```

It might take a bit, because it will pull services images and build the image for our app.

Among other logs from different services (Rabbitmq is writes especially a lot of logs) you will see these lines from the `web` process:

```
web_1       | Watching for file changes with StatReloader
web_1       | Performing system checks...
web_1       |
web_1       | System check identified no issues (0 silenced).
web_1       |
web_1       | You have 18 unapplied migration(s). Your project may not work properly until you apply the migrations for app(s): admin, auth, contenttypes, sessions.
web_1       | Run 'python manage.py migrate' to apply them.
web_1       | August 30, 2020 - 14:33:34
web_1       | Django version 3.1, using settings 'django_for_saas_app.settings'
web_1       | Starting development server at http://0.0.0.0:8090/
web_1       | Quit the server with CONTROL-C.
```

Try opening the link in browser: http://127.0.0.1:8090/

![image.png](./image.png)

Please note, if you will try opening http://0.0.0.0:8090/ link, you will get the error DisallowedHost, which is completely normal.

To stop the containers press CTRL+C.

For PyCharm Users.

You want your PyCharm to know what interpreter to use. 

Open IDE preferences, go to Project: django_for_saas_app -> Python Interpreter.

To the right of drop down click on the cog icon and click "Add" from the dropdown.

Find and click Docker Compose on the left side bar.

Configuration file(s) should be `./docker-compose.yml`

Service should be `web`.

Click OK.

![image_1.png](./image_1.png)

The Preferences window should look like this now:

![image_2.png](./image_2.png)

You can see that Python Interpreter says "Remote Python 3.8.5" and there is list of installed packages.

Click OK.

Now PyCharm will work with autocomplete based on our image.

Remember, every time you add new dependency in requirements.txt you should rebuild the image with `docker-compose build`.

That's it for this lesson!

Let's wrap up:
- We have created a Django project
- Set up our docker-compose to run the app and services
- The app is accessable by browser!



